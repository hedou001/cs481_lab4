﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace cs481_lab4
{
    public partial class MainPage : ContentPage
    {
        private bool enteringFirstNumber = true;
        private string op = "";
        private double leftNumber = 0.0;
        private double rightNumber = 0.0;
        public MainPage()
        {
            InitializeComponent();
        }

        private void Clear_Clicked(object sender, EventArgs e)
        {
            Display.Text = "0";
            rightNumber = 0.0;
            leftNumber = 0.0;
            op = "";
            enteringFirstNumber = true;
        }

        private void Number_Clicked(object sender, EventArgs e)
        {
            Button btnNbr = (Button)sender;
            int digit = Int32.Parse(btnNbr.Text);
            if (enteringFirstNumber)
            {
                if (leftNumber >= 0)
                    leftNumber = leftNumber * 10 + digit;
                else
                    leftNumber = leftNumber * 10 - digit;
                Display.Text = leftNumber.ToString();
            }
            else
            {
                if (rightNumber >= 0)
                    rightNumber = rightNumber * 10 + digit;
                else
                    rightNumber = rightNumber * 10 - digit;
                Display.Text = rightNumber.ToString();
            }
        }

        private void Operator_Clicked(object sender, EventArgs e)
        {
            Button btnOp = (Button)sender;
            if (btnOp.Text == "+" || btnOp.Text == "-" || btnOp.Text == "x" || btnOp.Text == "/" || btnOp.Text == "%")
            {
                if (op != "" && enteringFirstNumber == false)
                {
                    leftNumber = Calculate(leftNumber, rightNumber, op);
                    Display.Text = leftNumber.ToString();
                    op = btnOp.Text;
                    rightNumber = 0.0;
                    return;
                }
                op = btnOp.Text;
                enteringFirstNumber = false;
            }
            else if (btnOp.Text == "=")
            {
                leftNumber = Calculate(leftNumber, rightNumber, op);
                Display.Text = leftNumber.ToString();
                op = "";
                rightNumber = 0.0;
                enteringFirstNumber = true;
            }
            else if (btnOp.Text == "+/-")
            {
                if (enteringFirstNumber)
                {
                    leftNumber *= -1;
                    Display.Text = leftNumber.ToString();
                }
                else
                {
                    rightNumber *= -1;
                    Display.Text = rightNumber.ToString();
                }
            }
        }

        private double Calculate(double left, double right, string op)
        {
            switch (op)
            {
                case "+":
                    return left + right;
                case "-":
                    return left - right;
                case "x":
                    return left * right;
                case "/":
                    if (right == 0.0)
                        return 0.0;
                    return left / right;
                case "%":
                    if (right == 0.0)
                        return 0.0;
                    return left % right;
                default:
                    return left;
            }
        }
    }
}
